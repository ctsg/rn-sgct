import React from 'react';
import { Text, StyleSheet } from 'react-native';

const TitleText = props => {
  return (
    <Text style={{...styles.body, ...props.style}}>{props.children}</Text>
  );
}

const styles = StyleSheet.create({
  body: {
    fontFamily: 'nova',
    fontSize: 25,
  },
});

export default TitleText;
