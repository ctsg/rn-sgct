import React, { useState, useRef, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';

import Colors from '../constants/colors';
import BodyText from '../components/BodyText';
import TitleText from '../components/TitleText';
import MainButton from '../components/MainButton';

const GameOverScreen = props => {
  return (
    <View style={styles.screen}>
      <TitleText>The Game is Over!</TitleText>
      <View style={styles.imageContainer}>
      <Image
        fadeDuration={1000}
        source={require('../assets/road2success.jpeg')}
        style={styles.image}
        resizeMode="cover" />
      </View>
      <View style={styles.resultContainer}>
      <BodyText style={styles.resultText}>Your phone needed <Text style={styles.hilite}>{props.roundsNumber}
      </Text> rounds to guess <Text style={styles.hilite}>{props.userNumber}</Text></BodyText>
      </View>
      <MainButton onPress={props.onRestart}>NEW GAME</MainButton>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageContainer: {
    width: 300,
    height: 300,
    borderRadius: 150,
    borderWidth: 3,
    borderColor: 'black',
    overflow: 'hidden',
  },
  image: {
    width: '100%',
    height: '100%'
  },
  hilite: {
    color: Colors.primary,
    fontFamily: 'anon'
  },
  resultContainer: {
    marginVertical: 15,
    marginHorizontal: 30,
    width: '80%',
  },
  resultText: {
    fontSize: 20,
    textAlign: 'center',
  }
});

export default GameOverScreen;
